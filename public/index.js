//for client side of the front end usiing IIFE to execute application
(function () {
  // Create an instance of Angular application
  // 1st pramarater: app's name, it set the apps dependencies
  angular
    .module("FirstApp", [])
    .controller("FirstCtrl", [FirstCtrl]);

// Define a function
  function FirstCtrl() {
    var firstCtrl = this;

    firstCtrl.myText = "Say Hello to AngularJS on this back end text!!";
    firstCtrl.mynumber1 = 0;
    firstCtrl.mynumber2 = 0;
    firstCtrl.mynumber3 = 0;
    firstCtrl.myAnswer = 0;

    firstCtrl.addition = function(){
      firstCtrl.myAnswer = firstCtrl.mynumber1 + firstCtrl.mynumber2;
    }


    firstCtrl.reverse = function(){
      var reservedValues = firstCtrl.myText.split('').reverse().join('');
      console.log(reservedValues);
      firstCtrl.myText = reservedValues;
    }
  }

})();

/*
(function () {
    angular
        .module("FirstApp")
        .controller("FirstCtrl", ["$state", "AuthFactory", "Flash", 'ngProgressFactory', LoginCtrl]);

    function LoginCtrl($state, AuthFactory, Flash, ngProgressFactory){
        var vm = this;


    }
})();*/
